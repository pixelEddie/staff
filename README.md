"#Staff Plugin README" 


Description: 
This plugin adds a “Staff” section to the WordPress admin panel to easily show clients the people on your team. Quickly add staff members, pictures, position, email, phone number and display them on any page with a simple shortcode. 

FAQ
Step by step instructions:
1.	Goto the Staff icon on the left toolbar and then go to Department(s).
2.	Add a new Department. Add this shortcode to your page [our_team department=””]
3.	Goto to the Staff icon on the left toolbar and then click on Add New.
  -	Enter the Staff name where it says Enter title here in the top of the page. 
  -	Click on featured image to add an image of your team member.
  -	Fill out the rest of the Staff Info box.
  -	Add their Department and the order you want him/her to show up in. 0 will be shown first then 1, 2, etc per each Department.
4.	If you click on Staff, you can see all of your Staff from all of the Departments in a list table. 
  - Sort by Departments to quickly locate your staff member. 
  - You can quickly edit staff info by hovering over the name and clicking edit or delete. 
  - Under Departments, if you click the number under the Team Members column, you can quickly goto all the team members within that Department.


