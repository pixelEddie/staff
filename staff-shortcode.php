<?php
function esp_staff_taxonomy_list( $atts, $content = null ) {

	$atts = shortcode_atts(

		array(
			'title' => 'Current Staff Members in...'
		), $atts

	);


	$departments = get_terms( 'department' );

	if( ! empty( $departments ) && ! is_wp_error( $departments ) ) {

		$displaylist = '<div id="staff-department-list">';
		$displaylist .= '<h4>' . esc_html__( $atts[ 'title' ] ) . '</h4>';
		$displaylist .= '<ul>';

		foreach( $departments as $department ) {

			$displaylist .= '<li class="staff-department">';
			$displaylist .= '<a href="' . esc_url( get_term_link( $department ) ) . '">';
			$displaylist .= esc_html__( $department->name ) . '</a></li>';

		}

		$displaylist .= '</ul></div>';

	}
	
	return $displaylist;

}
add_shortcode( 'staff_department_list', 'esp_staff_taxonomy_list' );



function esp_list_our_team ( $atts, $content = null ) {

	if ( ! isset( $atts[ 'department' ] ) ) {
		return '<p class="staff-error">You must provide a department for this shortcode to work.</p>';
	}

	$atts = shortcode_atts( 
		array(
			'title' 		=>	'',
			// 'count' 		=> 	5,
			'department' 	=> 	'',
			'pagination' 	=> 	'off'
			
		), $atts );

	// Pagination $paged
	$pagination = $atts[ 'pagination' ] == 'on' ? false : true;
	$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;

	$args = array(
			'post_type' 	=> 	'staff',
			'post_status'	=>	'publish',
			'no_found_rows'	=>	$atts[ 'pagination' ],
			'post_per_page'	=>	$atts[ 'count' ],
			'paged'			=> 	$paged,
			'tax_query'		=>	array(
					array(
							'taxonomy'	=> 'department',
							'field'		=>	'slug',
							'terms'		=>	$atts[ 'department' ],	
					),
			),
			'orderby'		=>	'menu_order',
			'order'			=>	'ASC'
	);

	$our_team = new WP_Query( $args );

	if ( $our_team-> have_posts() ) : 
		$department = str_replace ( '-', ' ', $atts['department'] );

		$display_by_department = '<div id="staff-by-department">';
		$display_by_department .= '<h2 class="title-dept">' . esc_html__( $atts[ 'title' ] ) . '&nbsp' . esc_html__( ucwords ( $department ) ) . '</h4>'; 
		$display_by_department .= '<div>';
		$display_by_department .= '<div class="staff-listing row">';

		while ( $our_team->have_posts() ) : $our_team->the_post();

			global $post;

			$staffName = get_post_meta( get_the_ID(), 'staffName_id', true );
			$staffTitle = get_post_meta( get_the_ID(), 'staff_title', true );
			$staffEmail = get_post_meta( get_the_ID(), 'staff_email', true );
			$staffPhone = get_post_meta( get_the_ID(), 'staff_phone', true );
			$title = get_the_title();
			$slug = get_permalink();
			$staffImage = get_the_post_thumbnail();
			
		
		$display_by_department .= '<div class="col-sm-6 col-md-3 sCard">';
		// $display_by_department .= sprintf( '<a href="%s">%s</a>&nbsp&nbsp', esc_url( $slug ), esc_html__( $title )  );
		$display_by_department .= '<div class="box-border-lay">';	
		$display_by_department .= '<div class="sImage">' . ( $staffImage ) . '</div>&nbsp&nbsp';

		$display_by_department .= '<ul class="list-unstyled">';

		$display_by_department .= '<li class="staff-name">' . esc_html ( $staffName ) . '</li>';
		$display_by_department .= '<li class="staff-title">' . esc_html ( $staffTitle ) . '</li>';
		$display_by_department .= '<li class="staff-email"><a href="mailto:' . $staffEmail .'" class="staff-email">' . ( $staffEmail ) . '</a></li>';
		$display_by_department .= '<li class="staff-phone">' . esc_html ( $staffPhone ) . '</li>';

		$display_by_department .= '</ul>';

		$display_by_department .= '</div>';
		$display_by_department .= '</div>';

		endwhile;
		

		$display_by_department .= '</div>';

		$display_by_department .= '</div>';
		$display_by_department .= '</div>';

	else:
	
		$display_by_department = sprintf( __( '<p class="staff-error">Sorry, no staff listed in %s where found.</p>' ), esc_html__( ucwords( str_replace( '-', ' ', $atts[ 'department' ] ) ) ) );	

	endif;

	wp_reset_postdata();

	return $display_by_department;
	
}
add_shortcode( 'our_team', 'esp_list_our_team' );

