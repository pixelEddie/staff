<?php 

// Upcoming Feature
// function esp_add_submenu_page(){
// 	add_submenu_page(
// 		'edit.php?post_type=staff',
// 		__('Reorder Team Members'),
// 		__('Reorder Team Members'),
// 		'manage_options',
// 		'reorder_team_members',
// 		'reorder_admin_team_members_callback'
// 	);
// }

// add_action( 'admin_menu', 'esp_add_submenu_page' );

function reorder_admin_team_members_callback(){
	

	$args = array(
		'post_type' 				=> 'staff',
		'orderby' 					=> 'menu_order',
		'order'						=> 'ASC',
		'post_status'				=> 'publish',
		'no_found_rows'				=> true,
		'update_post_term_cache' 	=> false,
		'post_per_page'				=> 50
		// 'category_name' 	=> 'sales'
	);

	$staff_listing = new WP_Query( $args );
	
	?>

	<div id="staff-sort" class="wrap">
		<div id="icon-staff-admin" class="icon32"><br /></div>
		<h2><?php _e( 'Sort Team Members', 'staff' ); ?><img src="<?php echo esc_url( admin_url() . '/images/loading.gif' ); ?>" id="loading-animation" /></h2> 
			<?php if ( $staff_listing->have_posts() ) : ?>
				<p><?php _e( '<strong>Note: </strong>this only affects the staff listed using the shortcode functions.', 'staff' ); ?></p>
				<ul id="custom-type-list">
					<?php while ( $staff_listing->have_posts() ) : $staff_listing->the_post(); ?>
						<li id="<?php esc_attr( the_id() ); ?>"><?php esc_html( the_title() ); ?></li>
					<?php endwhile; ?>
				</ul>
			<?php else: ?>
				<p><?php _e( 'You have no Staff to sort.', 'staff' ); ?></p>
			<?php endif; ?>
	</div>

	<?php

}

function esp_save_reorder() {

	if ( ! check_ajax_referer( 'wp-staff-order', 'security' ) ) {
		return wp_send_json_error( 'Invalid Nonce' );
	}

	if ( ! current_user_can( 'manage_options' ) ) {
		return wp_send_json_error( 'You are not allowed to do this.' );
	}

	$order = $_POST['order'];
	$counter = 0;

	foreach( $order as $item_id ) {

		$post = array(
			'ID' 			=> (int)$item_id,
			'menu_order' 	=> $counter,

		);

		wp_update_post( $post );

		$counter++;
	}

	wp_send_json_success( 'Post Saved.' );

}
add_action( 'wp_ajax_save_sort', 'esp_save_reorder' );