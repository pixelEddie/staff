<?php 
/**
* Plugin Name: WP Staff
* Description: A plugin for creating and displaying your staff.
* Author: Eddie Jung
* Version: 1.0.0
*/

//Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

require_once ( plugin_dir_path(__FILE__) . 'staff-cpt.php' );
require_once ( plugin_dir_path(__FILE__) . 'staff-render-admin.php' );
require_once ( plugin_dir_path(__FILE__) . 'staff-fields.php' );
require_once ( plugin_dir_path(__FILE__) . 'staff-settings.php' );
require_once ( plugin_dir_path(__FILE__) . 'staff-shortcode.php' );

function esp_admin_enqueue_scripts() {
	global $pagenow, $typenow;

	if ( $typenow == 'staff' ) {
		wp_enqueue_style( 'esp-admin-css', plugins_url( 'css/admin-staff.css', __FILE__ ) );

	}

	if ( ( $pagenow == 'post.php' || $pagenow == 'post-new.php' ) && $typenow == 'staff' ){
		
		wp_enqueue_script( 'esp-admin-js', plugins_url( 'js/admin-staff.js', __FILE__  ), array( 'jquery'), '20170110', true );
		wp_enqueue_script( 'esp-custom-quicktags', plugins_url( 'js/esp-quicktags.js', __FILE__ ), array( 'quicktags'), '20170110', true );
	}

	

	// Upcoming feature 	
	// if ( $pagenow == 'edit.php' && $typenow == 'staff' ) {
	// 	wp_enqueue_script( 'reorder-js', plugins_url( 'js/reorder.js', __FILE__  ), array( 'jquery', 'jquery-ui-sortable' ), '20170112', true );
	// 	wp_localize_script( 'reorder-js', 'STAFF_LISTING', array(
	// 		'security' => wp_create_nonce( 'wp-staff-order' ),
	// 		'success'  => __( 'Staff Team Member sort order has been saved.' ),
	// 		'failure'  => __( 'There was an error saving the sort order or you do not have the proper permissions.' )
	// 	) );	

	// }

}
add_action( 'admin_enqueue_scripts', 'esp_admin_enqueue_scripts' );

// Adding /admin-staff.js for Add New Staff Page in the Admin Panel.
add_action( 'admin_print_scripts-post-new.php', 'esp_staff_input_admin_script', 11 );
add_action( 'admin_print_scripts-post.php', 'esp_staff_input_admin_script', 11 );

function esp_staff_input_admin_script() {
    global $post_type;
    if( 'staff' == $post_type )
    wp_enqueue_script( 'esp_staff_input_admin_script', plugins_url( 'js/admin-staff.js', __FILE__  ), array( 'jquery'), '20170206', true );
}

function esp_prefix_enqueue() 
{       
	 // // JQuery
  //   wp_register_script('prefix_jquery', 'https://code.jquery.com/jquery-3.1.1.min.js');
  //   wp_enqueue_script('prefix_jquery');

  //   // JS
  //   wp_register_script('prefix_bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js');
  //   wp_enqueue_script('prefix_bootstrap');

  //   // CSS
  //   wp_register_style('prefix_bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css');
  //   wp_enqueue_style('prefix_bootstrap');

    wp_enqueue_style( 'esp-admin-css', plugins_url( 'css/admin-staff.css', __FILE__ ) );
}
add_action( 'wp_enqueue_scripts', 'esp_prefix_enqueue' );

