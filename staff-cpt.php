<?php

function esp_register_post_type(){

	$singular = 'Staff';
	$plural = 'Staff';	

	$labels = array(
		'name' 					=> $plural,
		'singular_name' 		=> $singular,
		'add_name' 				=> 'Add New',
		'add_new_item' 			=> 'Add New ' . $singular,
		'edit' 					=> 'Edit',
		'edit_item' 			=> 'Edit ' . $singular,
		'new_item' 				=> 'New ' . $singular,
		'view' 					=> 'View ' . $singular,
		'view_item' 			=> 'View ' . $singular,
		'search_term' 			=> 'Search ' . $plural,
		'parent' 				=> 'Parent ' . $singular,
		'not_found' 			=> 'No ' . $plural .' found',
		'not_found_in_trash'	=> 'No ' . $plural .' in Trash'	
	);	

	$args = array( 
		'labels' 				=> $labels,
		'public' 				=> true,
		'publicly_queryable' 	=> true,
		'exclude_from_search' 	=> false,
		'show_in_nav_menus' 	=> true,
		'show_ui' 				=> true,
		'show_in_menu' 			=> true,
		'show_in_admin_bar' 	=> true,
		'menu_position' 		=> 25,
		'menu_icon' 			=> 'dashicons-businessman',
		'can_export' 			=> true,
		'delete_with_user' 		=> false,
		'hierarchical' 			=> false,
		'has_archive' 			=> true,
		'query_var' 			=> true,
		'capability_type' 		=> 'page',
		'map_meta_cap' 			=> true,
		// 'capabilities' 		=> array(),
		'rewrite' 				=> array(
			'slug' 				=> 'staff',
			'with_front' 		=> true,
			'pages' 			=> true,
			'feeds' 			=> false,

		),
		'supports'				=> array(
			'title',
			// 'editor',
			// 'author',
			'custom-fields',
			'thumbnail',
			'page-attributes'
		)
	 );	

	register_post_type( 'staff', $args );
}
add_action( 'init', 'esp_register_post_type' );


function esp_register_taxonomy(){

	$plural = 'Departments';
	$singular = 'Department';

	$labels = array(
		'name'                       => $plural,
        'singular_name'              => $singular,
        'search_items'               => 'Search ' . $plural,
        'popular_items'              => 'Popular ' . $plural,
        'all_items'                  => 'All ' . $plural,
        'parent_item'                => null,
        'parent_item_colon'          => null,
        'edit_item'                  => 'Edit ' . $singular,
        'update_item'                => 'Update ' . $singular,
        'add_new_item'               => 'Add New ' . $singular,
        'new_item_name'              => 'New ' . $singular . ' Name',
        'separate_items_with_commas' => 'Separate ' . $plural . ' with commas',
        'add_or_remove_items'        => 'Add or remove ' . $plural,
        'choose_from_most_used'      => 'Choose from the most used ' . $plural,
        'not_found'                  => 'No ' . $plural . ' found.',
        'menu_name'                  => $plural
	);

	$args = array(
		'hierarchical'			=> false,
		'description'			=> '',
		'labels'				=> $labels,
		'show_ui'				=> true,
		'show_admin_column'		=> true,
		'update_count_callback'	=> '_update_post_term_count',
		'query_var'				=> true,
		'rewrite'				=> array( 'slug' => 'department' )
	);


	register_taxonomy( 'department', 'staff', $args );
}
add_action( 'init', 'esp_register_taxonomy' );

// Adding additional fields to staff listing table wp-admin
function my_cpt_columns( $columns ) {
    $columns["staff_title"] = "Position";
    $columns["staff_email"] = "Email";
    $columns["staff_phone"] = "Phone";
    $columns["menu_order"]	= "Order";
    return $columns;
}
add_filter('manage_edit-staff_columns', 'my_cpt_columns');
add_filter('manage_edit-staff_sortable_columns', 'my_cpt_columns');

// Reorder columns in staff-listing
function my_cpt_columns_reorder( $columns ) {
	$position 	= $columns['staff_title'];
	$date 		= $columns['date'];
    $department = $columns['taxonomy-department'];
    $email 		= $columns['staff_email'];
    $phone 		= $columns['staff_phone'];
    $order 		= $columns["menu_order"];
 
    unset( $columns['taxonomy-department'], $columns['date'], $columns['staff_email'], $columns['staff_phone'], $columns['staff_title'], $columns['menu_order'] );

    $columns['menu_order'] 			= $order;
    $columns['taxonomy-department'] = $department;
    $columns['staff_title'] 		= $position;
    $columns['staff_email'] 		= $email;
    $columns['staff_phone'] 		= $phone;
    return $columns;
 }
 add_filter('manage_edit-staff_columns', 'my_cpt_columns_reorder');
// Move date column to right of our custom column

// Output Table Cell Contents
function my_cpt_column( $colname, $cptid ) {
     if ( $colname == 'staff_title')
          echo get_post_meta( $cptid, 'staff_title', true );
     if ( $colname == 'staff_email')
          echo get_post_meta( $cptid, 'staff_email', true );
     if ( $colname == 'staff_phone')
          echo get_post_meta( $cptid, 'staff_phone', true );
     if ( $colname == 'menu_order')
          echo get_post_meta( $cptid, 'menu_order', true );  

}
add_action('manage_staff_posts_custom_column', 'my_cpt_column', 10, 2);
// The actual column data's output

// Sort by Name
function my_sort_metabox( $vars ) {
      if( array_key_exists('orderby', $vars )) {
           if('Name' == $vars['orderby']) {
                $vars['orderby'] 	= 'meta_value';
                $vars['meta_key'] 	= 'staffName_id';
           }
      }
      return $vars;
}
add_filter('request', 'my_sort_metabox');
// The ASC and DESC part of ORDER BY is handled automatically


function esp_my_cpt_columns( $columns ) {
    $columns["title"] = "Name";
    return $columns;
}
add_filter('manage_edit-staff_columns', 'esp_my_cpt_columns');

// Remove views in admin wp list table for staff 
add_filter( 'post_row_actions', 'remove_row_actions', 10, 1 );
function remove_row_actions( $actions )
{
    if( get_post_type() === 'staff' )
        
        unset( $actions['view'] );

    return $actions;
}


// Remove department description column in department listing
add_filter( 'manage_edit-department_columns', 'departmentColumns' );
function departmentColumns( $columns ) {    
    if ( isset( $columns[ 'description' ] ) ) {
        unset( $columns[ 'description' ] );
    }
    // Change the name of descriptions to Team Members in the table
    $columns["posts"] = "Team Members";
    // Change the name of slug to Type in the table
    $columns["slug"]  = "Shortcode";

    return $columns;        
}

// Reorder columns in department listing
function departmentColumnsReorder( $columns ) {
	
	$staff = $columns['posts'];
	$slug = $columns['slug'];
 
    unset( $columns['posts'], $columns['slug'] );

    $columns['posts'] = $staff;
    $columns['slug'] = $slug;

    return $columns;
 }
 add_filter('manage_edit-department_columns', 'departmentColumnsReorder');

// Adjust width and alignment of Departments WP list table for team members and slug columns.
 add_action( 'admin_print_styles', 'cor_admin_print_styles', 200 );
function cor_admin_print_styles() {
    $screen = get_current_screen();
    if ( 'edit-tags' === $screen->base && 'department' === $screen->taxonomy ) {
        echo <<<EOF
<style>
    .fixed .column-posts {
        width: 25%;
    }
    .widefat .num, .column-comments, .column-links, .column-posts {
        text-align: left;
    }
</style>
EOF;
    }
}

// remove views in admin wp list table for department
add_filter( 'department_row_actions', 'remove_row_taxonomy_actions', 10, 1 );
function remove_row_taxonomy_actions( $actions ) {
    
    if( get_taxonomy('department') )
        
        unset( $actions['view'] );

    return $actions;

}

// Featured Preview image size when adding/editing a staff member
add_action( 'init', 'esp_add_new_image_size' );
function esp_add_new_image_size() {
	add_theme_support( 'post-thumbnails' );
	// add_image_size('thumbnail', 50, 50, true);
	the_post_thumbnail('medium');
    set_post_thumbnail_size( 'image_size', 150, 150, true ); 

}


// Allow custom order column values for Team Order # in admin panel for Team Member
function esp_show_order_column($name){
  global $post;

  switch ($name) {
    case 'menu_order':
      $order = $post->menu_order;
      echo $order;
      break;
   default:
      break;
   }
}
add_action('manage_staff_posts_custom_column','esp_show_order_column');


// Make column for Team Order # sortable
function esp_order_column_register_sortable($columns){
  $columns['menu_order'] = 'menu_order';
  $columns['taxonomy-department'] = 'taxonomy-department';
  return $columns;
}
add_filter('manage_edit-staff_sortable_columns','esp_order_column_register_sortable');

// This sorts the taxonomy department by ascending or descending order in the staff list table admin.
if(!function_exists( 'esp_sort_custom_column' ) ) {
    function esp_sort_custom_column( $clauses, $wp_query ) {
        global $wpdb;
        if(isset($wp_query->query['orderby']) && $wp_query->query['orderby'] == 'taxonomy-department'){
            $clauses['join'] .= <<<SQL
LEFT OUTER JOIN {$wpdb->term_relationships} ON {$wpdb->posts}.ID={$wpdb->term_relationships}.object_id
LEFT OUTER JOIN {$wpdb->term_taxonomy} USING (term_taxonomy_id)
LEFT OUTER JOIN {$wpdb->terms} USING (term_id)
SQL;
            $clauses['where'] .= "AND (taxonomy = 'department' OR taxonomy IS NULL)";
            $clauses['groupby'] = "object_id";
            $clauses['orderby'] = "GROUP_CONCAT({$wpdb->terms}.name ORDER BY name ASC)";
            if(strtoupper($wp_query->get('order')) == 'ASC'){
                $clauses['orderby'] .= 'ASC';
            } else{
                $clauses['orderby'] .= 'DESC';
            }
        }
        return $clauses;
    }
    add_filter( 'posts_clauses', 'esp_sort_custom_column', 10, 2 );
}

// This hides the custom meta box by default

function esp_default_hidden_meta_boxes( $hidden, $screen ) {
	// Grab the current post type
	$post_type = $screen->post_type;
	// If we're on a 'post'...
	if ( $post_type == 'post' ) {
		// Define which meta boxes we wish to hide
		$hidden = array(
			'postcustom-hide',
		);
		// Pass our new defaults onto WordPress
		return $hidden;
	}
	// If we are not on a 'post', pass the
	// original defaults, as defined by WordPress
	return $hidden;
}
add_action( 'default_hidden_meta_boxes', 'esp_default_hidden_meta_boxes', 10, 2 );


// Display Thumbnail in staff section of wp admin staff list table
add_filter( 'manage_edit-staff_columns', 'esp_posts_columns', 5 );
add_action( 'manage_posts_custom_column', 'esp_posts_custom_columns', 5, 2 );
function esp_posts_columns( $defaults ){
    $defaults['riv_post_thumbs'] = __('Thumbs');
    return $defaults;
}
function esp_posts_custom_columns( $column_name, $id ) {
        if( $column_name === 'riv_post_thumbs' ) {
        echo the_post_thumbnail( array( 50, 50 ) );
    }
}

// Remove WP Node 'View' in admin section ow wp admin bar
add_action( 'admin_bar_menu', 'remove_wp_logo', 999 );
function remove_wp_logo( $wp_admin_bar ) {
	if( get_post_type() === 'staff' ){
	$wp_admin_bar->remove_node( 'view' );
	$wp_admin_bar->remove_node( 'archive' );
	}
}


// Remove Yoast meta box for custom post type Staff
function remove_yoast_metabox_reservations(){
    remove_meta_box('wpseo_meta', 'staff', 'normal');
}
add_action( 'add_meta_boxes', 'remove_yoast_metabox_reservations',11 );
 


// Remove Yoast columns in staff wp list table 
function rkv_remove_columns( $columns ) {
	// remove the Yoast SEO columns
	unset( $columns['wpseo-score'] );
	unset( $columns['wpseo-title'] );
	unset( $columns['wpseo-metadesc'] );
	unset( $columns['wpseo-focuskw'] );
	unset( $columns['wpseo-score-readability'] );
	
	return $columns;
}
add_filter ( 'manage_edit-staff_columns', 'rkv_remove_columns' );



// Hide the term description(box) and Yoast Meta in the post_tag edit form
add_action( "department_edit_form", function( $tag, $taxonomy )
{ 
	if( get_taxonomy('department') ){
        
        ?><style>.term-description-wrap{display:none;}#wpseo_meta{display: none;}</style><?php

}
    
}, 10, 2 );



// Remove Description box in Departments
add_action( 'admin_footer-edit-tags.php', 'wpse_56569_remove_cat_tag_description' );
function wpse_56569_remove_cat_tag_description(){
    global $current_screen;
    switch ( $current_screen->id ) 
    {
        case 'edit-category':
            // WE ARE AT /wp-admin/edit-tags.php?taxonomy=category
            // OR AT /wp-admin/edit-tags.php?action=edit&taxonomy=category&tag_ID=1&post_type=post
            break;
        case 'edit-post_tag':
            // WE ARE AT /wp-admin/edit-tags.php?taxonomy=post_tag
            // OR AT /wp-admin/edit-tags.php?action=edit&taxonomy=post_tag&tag_ID=3&post_type=post
            break;
    }
    ?>
    <script type="text/javascript">
    jQuery(document).ready( function($) {
        $('#tag-description').parent().remove();

        // This will automatically input the New Department "name" into the "slug" input. 
        $("#tag-name").keyup(function(){
	    update();
		});
		function update() {
		  $("#tag-slug").val($("#tag-name").val());
		}

		// Change the "Slug" label in Departments to "Type" label
		$('.term-slug-wrap label').text('Type');
		$('.term-slug-wrap p').text('The “type” is the URL-friendly version of the name. It is usually all lowercase and contains only letters, numbers, and hyphens.');

        // Add Shortcode brackets and display shortcode in Departments Admin section
        $(".slug").prepend('[our_team department="');
        $(".slug").append('"]');

        $("#submit").on("click", function(){ 
            window.location.reload(); 
        });
    });
    </script>
    <?php
}

// Reorder Staff submenu items in admin menu
add_filter( 'custom_menu_order', 'wpse_73006_submenu_order' );
function wpse_73006_submenu_order( $menu_ord ) 
{
    global $submenu;

    // Enable the next line to inspect the $submenu values
    // echo '<pre>'.print_r($submenu,true).'</pre>';

    $arr = array();
    $arr[] = $submenu['edit.php?post_type=staff'][15];
    $arr[] = $submenu['edit.php?post_type=staff'][5];
    $arr[] = $submenu['edit.php?post_type=staff'][10];
    $submenu['edit.php?post_type=staff'] = $arr;

    return $menu_ord;
}


// Setting the Meta box order for Add new staff/ Edit Staff in the Admin
add_filter( 'get_user_option_meta-box-order_staff', 'metabox_order' );
function metabox_order( $order ) {
	if ( get_post_type() === 'staff' ){
	    return array(
	        'normal' => join( 
	            ",", 
	            array(       // vvv  Arrange here as you desire
	                'postimagediv',
	                'esp_meta',
	                'tagsdiv-department',
	                'pageparentdiv',
	            )
	        ),
	    );
	}
}

// Remove Custom Field and Slug meta boxes for custom post type Staff
function remove_more_metabox_reservations(){
    remove_meta_box('postcustom', 'staff', 'normal');
    remove_meta_box('slugdiv', 'staff', 'normal');
}
add_action( 'add_meta_boxes', 'remove_more_metabox_reservations',11 );

// ************************************************Currently in development*****************************************************
// Add shortcode column to post list and show shortcode in the Department list table
// add_action( 'manage_department_custom_column' , 'department_custom_columns', 10, 2 );
// function department_custom_columns( $column, $post_id ) {
//     switch ( $column ) {
// 	case 'slug' :
// 		global $post;
// 		// $slug = '' ;
// 		// $slug = $post->post_slug;
//   //       $shortcode = '<span style="display:inline-block;border:solid 2px lightgray; background:white; padding:0 8px; font-size:13px; line-height:25px; vertical-align:middle;">[our_team department="'.$slug.'"]</span>';
// 	 //    echo $shortcode;

//         /* Get the post meta */
//      $slug = get_post_meta( $post_id, 'slug', true );
        
//         /* If no duration is found, output a default message. */
//             if ( empty( $slug ) ) 
//                 echo __( 'Unknown' );

//         /* If there is a duration, append 'minutes' to the text string. */
//             else
//                 printf( __( '<span style="display:inline-block;border:solid 2px lightgray; background:white; padding:0 8px; font-size:13px; line-height:25px; vertical-align:middle;">[our_team department="'.$slug.'"]</span>' ) );


// 	    break;
//     }
// }

// Adding shortcode column
// add_filter('manage_edit-department_columns' , 'add_department_columns');
// function add_department_columns($columns) { return array_merge($columns, array('shortcode' => 'Shortcode')); }


// function add_book_place_column_content($content,$column_name,$term_id){
//     $term= get_term($term_id, 'department');
//     switch ($column_name) {
//         case 'shortcode':

//         global $post;
//         $post_slug=$post->post_name;
//             //do your stuff here with $term or $term_id
//             $content = $post_slug;
//             break;
//         default:
//             break;
//     }
//     return $content;
// }
// add_filter('manage_department_custom_column', 'add_book_place_column_content',10,3);








