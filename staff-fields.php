<?php 

function esp_add_custom_metabox() {

	add_meta_box(
		'esp_meta',
		__( 'Staff Info' ),
		'esp_meta_callback',
		'staff',
		'normal',
		'core' 
	);
}
add_action( 'add_meta_boxes', 'esp_add_custom_metabox' );

function esp_meta_callback( $post ){
	wp_nonce_field( basename( __FILE__ ), 'esp_staff_nonce' );
	$esp_stored_meta = get_post_meta( $post->ID ); ?>

	<div>
		<div class="meta-row">
			<div class="meta-th">
				<label for="staffName-id" class="esp-row-title"><?php _e('Name', 'staff'); ?></label>
			</div>
			<div class="meta-td">
				<input type="text" class="esp-row-content" name="staffName_id" id="staffName-id" value="<?php if ( ! empty ( $esp_stored_meta['staffName_id'] ) ) {
					echo esc_attr( $esp_stored_meta['staffName_id'][0] );
				} ?>" style="width:250px;" />
			</div>
		</div>
		<div class="meta-row">
			<div class="meta-th">
				<label for="staff-title" class="esp-row-title"><?php _e('Title', 'staff'); ?></label>
			</div>
			<div class="meta-td">
				<input type="text" name="staff_title" id="staff-title" value="<?php if ( ! empty ( $esp_stored_meta['staff_title'] ) ) {
					echo esc_attr( $esp_stored_meta['staff_title'][0] );
				} ?>" style="width:250px;" />
			</div>
		</div>
		<div class="meta-row">
			<div class="meta-th">
				<label for="staff-email" class="esp-row-title"><?php _e('Email', 'staff'); ?></label>
			</div>
			<div class="meta-td">
				<input type="email" name="staff_email" id="staff-email" value="<?php if ( ! empty ( $esp_stored_meta['staff_email'] ) ) {
					echo esc_attr( $esp_stored_meta['staff_email'][0] );
				} ?>" style="width:250px;" />
			</div>
		</div>
		<div class="meta-row">
			<div class="meta-th">
				<label for="staff-phone" class="esp-row-title"><?php _e('Phone', 'staff'); ?></label>
			</div>
			<div class="meta-td">
				<input type="tel" name="staff_phone" id="staff-phone
				" value="<?php if ( ! empty ( $esp_stored_meta['staff_phone'] ) ) {
					echo esc_attr( $esp_stored_meta['staff_phone'][0] );
				} ?>" style="width:250px;" />
			</div>
		</div>
	</div>

	<?php
}

function esp_meta_save( $post_id ){
	// Checks save status
    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'esp_staff_nonce' ] ) && wp_verify_nonce( $_POST[ 'esp_staff_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
    // Exits script depending on save status
    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
        return;
    }

    if ( isset( $_POST['staffName_id'] ) ) {
    	update_post_meta( $post_id, 'staffName_id', sanitize_text_field( $_POST[ 'staffName_id' ] ) );
    }

    if ( isset( $_POST['staff_title'] ) ) {
    	update_post_meta( $post_id, 'staff_title', sanitize_text_field( $_POST[ 'staff_title' ] ) );
    }

    if ( isset( $_POST['staff_email'] ) ) {
    	update_post_meta( $post_id, 'staff_email', sanitize_text_field( $_POST[ 'staff_email' ] ) );
    }

    if ( isset( $_POST['staff_phone'] ) ) {
    	update_post_meta( $post_id, 'staff_phone', sanitize_text_field( $_POST[ 'staff_phone' ] ) );
    }
}
add_action( 'save_post', 'esp_meta_save' );